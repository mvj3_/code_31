//截取字符
function utf_substr($str, $len = 10) {
	$nstr = $str;
	for($i = 0; $i < $len; $i ++) {
		$temp_str = substr ( $nstr, 0, 1 );
		if (ord ( $temp_str ) > 127) {
			$i ++;
			if ($i < $len) {
				$new_str [] = substr ( $nstr, 0, 3 );
				$nstr = substr ( $nstr, 3 );
			}
		} else {
			$new_str [] = substr ( $nstr, 0, 1 );
			$nstr = substr ( $nstr, 1 );
		}
	}
	if (strlen ( $str ) > $len)
		$new_str [] = '...';
	return join ( $new_str );
}